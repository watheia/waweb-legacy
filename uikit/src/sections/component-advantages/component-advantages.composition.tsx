/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { ComponentsAdvantages } from "./component-advantages"

export const ComponentsAdvantagesExample = () => (
  <ThemeProvider>
    <ComponentsAdvantages data-testid="test-advantages" />
  </ThemeProvider>
)

ComponentsAdvantagesExample.canvas = {
  width: 1400,
  height: 400,
  overflow: "scroll",
}
