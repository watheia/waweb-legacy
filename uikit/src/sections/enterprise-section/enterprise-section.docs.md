---
labels: ["react", "typescript", "home page", "cloud"]
description: "home page cloud section"
---

<!-- @format -->

### Overview

A section of static content, presenting Watheia Labs's cloud platform.  
Assumes the consuming component to supply className with width and other styles.  
[Link to page](https://watheia.app).
