/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { CtaSection } from "./cta-section"

export const CtaSectionExample = () => (
  <ThemeProvider>
    <CtaSection data-testid="test-cta" />
  </ThemeProvider>
)

CtaSectionExample.canvas = {
  height: 70,
}
