/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { brands } from "@waweb/uikit.theme.brands"
import { CommunitySection } from "./community-section"

export const CommunitySectionExample = () => (
  <ThemeProvider className={brands}>
    <CommunitySection githubStars={12600} data-testid="community-test" />
  </ThemeProvider>
)

CommunitySectionExample.canvas = {
  height: 430,
}
