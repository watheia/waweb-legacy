/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { CarouselSection } from "./carouselSection"

export const CarouselSectionExample = () => (
  <ThemeProvider
    style={{ display: "block", width: 1400, maxWidth: "100vw", minHeight: 410 }}
  >
    <CarouselSection data-testid="test-carousel" />
  </ThemeProvider>
)
