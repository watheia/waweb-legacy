---
labels: ["react", "typescript", "home page", "documentation"]
description: "home page documentation section"
---

<!-- @format -->

## Overview

A section of static content, detailing the Watheia Labs's documentation features.  
Assumes the consuming component to supply a className, with width and other styles.  
[Link to page](https://watheia.app).
