/** @format */

export type { SectionProps } from "./SectionProps"
export { default, default as Section } from "./Section"
