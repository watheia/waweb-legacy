import React from 'react';
import { ServerError } from './server-error';

export const BasicServerError = () => (
  <ServerError text="hello from ServerError" />
);
