/** @format */

import React from "react"
import { render } from "@testing-library/react"
import { WithLightTheme, WithDarkTheme } from "./not-found.composition"

it("should render with the the dark theme", () => {
  const { getByTestId } = render(<WithDarkTheme />)
  const rendered = getByTestId("with-dark-theme")
  expect(rendered).toBeTruthy()
})

it("should render with the the light theme", () => {
  const { getByTestId } = render(<WithLightTheme />)
  const rendered = getByTestId("with-light-theme")
  expect(rendered).toBeTruthy()
})
