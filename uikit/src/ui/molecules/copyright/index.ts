/** @format */

export { Copyright, Copyright as default } from "./copyright"
export type { CopyrightProps } from "./copyright"
