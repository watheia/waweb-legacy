/** @format */

import { Box, Button } from "@material-ui/core"
import React from "react"
import SectionHeader from "./section-header"
import { Image } from "@waweb/uikit.elements.image"

export const Example1 = () => (
  <Box
    marginBottom={2}
    display="flex"
    justifyContent="space-evenly"
    alignItems="center"
    padding={2}
    border="1px solid #ccc"
    borderRadius="4px"
  >
    <SectionHeader
      title="We are re-imagining renting to help you achieve your dreams"
      subtitle="Our mission is to help you grow your business, meet and connect with people who share your passions. We help you fulfill your best potential through an engaging lifestyle experience."
    />
  </Box>
)

export const Example2 = () => (
  <Box
    marginBottom={2}
    display="flex"
    justifyContent="space-evenly"
    alignItems="center"
    padding={2}
    border="1px solid #ccc"
    borderRadius="4px"
  >
    <SectionHeader
      title="We are reimagining renting to help you achieve your dreams"
      subtitle="Our mission is to help you grow your business, meet and connect with people who share your passions. We help you fulfill your best potential through an engaging lifestyle experience."
      align="left"
      fadeUp
    />
  </Box>
)

export const Example4 = () => (
  <Box
    marginBottom={2}
    display="flex"
    justifyContent="space-evenly"
    alignItems="center"
    padding={2}
    border="1px solid #ccc"
    borderRadius="4px"
  >
    <SectionHeader title="We are reimagining renting to help you achieve your dreams" />
  </Box>
)

export const Example5 = () => (
  <Box
    marginBottom={2}
    display="flex"
    justifyContent="space-evenly"
    alignItems="center"
    padding={2}
    border="1px solid #ccc"
    borderRadius="4px"
  >
    <SectionHeader
      title="We are reimagining renting to help you achieve your dreams"
      subtitle="Our mission is to help you grow your business, meet and connect with people who share your passions. We help you fulfill your best potential through an engaging lifestyle experience."
      ctaGroup={[
        <Button color="primary" variant="contained">
          Submit
        </Button>,
        <Button color="primary" variant="outlined">
          Learn More
        </Button>,
      ]}
    />
  </Box>
)

export const Example6 = () => (
  <Box
    marginBottom={2}
    display="flex"
    justifyContent="space-evenly"
    alignItems="center"
    padding={2}
    border="1px solid #ccc"
    borderRadius="4px"
  >
    <SectionHeader
      title={
        <>
          Find a job you love.<a href="https://watheia.app/"> Learn more</a>
        </>
      }
      subtitle={
        <>
          Try it now.<a href="https://watheia.app/"> Learn more</a>
        </>
      }
      ctaGroup={[
        <Button color="primary" variant="contained">
          Submit
        </Button>,
        <Button color="primary" variant="outlined">
          Learn More
        </Button>,
      ]}
    />
  </Box>
)

export const Example7 = () => (
  <Box
    marginBottom={2}
    display="flex"
    justifyContent="space-evenly"
    alignItems="center"
    padding={2}
    border="1px solid #ccc"
    borderRadius="4px"
  >
    <SectionHeader
      label="our process"
      title={
        <>
          Find a job you love.<a href="https://watheia.app/"> Learn more</a>
        </>
      }
      subtitle={
        <>
          Try it now.<a href="https://watheia.app/"> Learn more</a>
        </>
      }
      ctaGroup={[
        <Button color="primary" variant="contained">
          Submit
        </Button>,
        <Button color="primary" variant="outlined">
          Learn More
        </Button>,
      ]}
    />
  </Box>
)

export const Example8 = () => (
  <Box
    marginBottom={2}
    display="flex"
    justifyContent="space-evenly"
    alignItems="center"
    padding={2}
    border="1px solid #ccc"
    borderRadius="4px"
  >
    <SectionHeader
      overline={
        <Image
          src="https://cdn.watheia.org/assets/illustrations/rated-by-our-customer.png"
          alt="rating"
          style={{ width: 120, height: "auto" }}
          width="auto"
        />
      }
      title="Rated 5 out of 5 stars by our customers!"
      subtitle="Companies from across the globe have had fantastic experiences using Watheia. Here’s what they have to say."
      ctaGroup={[
        <Button color="primary" variant="contained">
          Submit
        </Button>,
        <Button color="primary" variant="outlined">
          Learn More
        </Button>,
      ]}
    />
  </Box>
)
