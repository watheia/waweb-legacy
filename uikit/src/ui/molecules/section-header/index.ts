/** @format */

export type { SectionHeaderProps } from "./section-header"
export { default, default as SectionHeader } from "./section-header"
