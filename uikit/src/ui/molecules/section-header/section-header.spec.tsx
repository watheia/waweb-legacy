/** @format */

import React from "react"
import { render } from "@testing-library/react"
import { Example1 } from "./section-header.composition"

it("should render with the the dark theme", () => {
  const { getByText } = render(<Example1 />)
  const rendered = getByText("We are re-imagining renting to help you achieve your dreams")
  expect(rendered).toBeTruthy()
})
