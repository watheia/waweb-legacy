/** @format */

import { MuiThemeProvider } from "@material-ui/core"
import React, { HTMLAttributes } from "react"
import { createTheme, ColorScheme, Theme } from "./theme"
import { Provider, defaultTheme } from "@adobe/react-spectrum"
import "./sass/main.scss"

export type ThemeProviderProps = {
  /**
   * primary color of theme.
   */
  colorScheme?: ColorScheme
} & HTMLAttributes<HTMLDivElement>

export function ThemeProvider({
  colorScheme,
  children,
  ...props
}: ThemeProviderProps) {
  const theme = createTheme(colorScheme)
  return (
    <div {...props}>
      <Theme.Provider value={{ colorScheme }}>
        <MuiThemeProvider theme={theme}>
          <Provider theme={defaultTheme} colorScheme={colorScheme}>
            {children}
          </Provider>
        </MuiThemeProvider>
      </Theme.Provider>
    </div>
  )
}
