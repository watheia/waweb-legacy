/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { Link } from "./link"

export const LinkExample = () => (
  <ThemeProvider>
    <Link href="https://google.com" data-testid="test-link">
      look it up!
    </Link>
  </ThemeProvider>
)

export const LinkWithExternal = () => (
  <ThemeProvider>
    <Link href="https://google.com" external data-testid="test-link">
      look it up!
    </Link>
  </ThemeProvider>
)

const compositions = [LinkExample, LinkWithExternal]
// @ts-ignore
compositions.map((comp) => (comp.canvas = { height: 90 }))
