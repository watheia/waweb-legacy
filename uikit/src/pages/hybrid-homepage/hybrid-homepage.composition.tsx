/** @format */

import React from "react"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"
import { brands } from "@waweb/uikit.theme.brands"
import { HybridHomepage } from "./hybrid-homepage"

export const HybridHomePageComposition = () => (
  <ThemeProvider className={brands}>
    <HybridHomepage
      githubStars={12600}
      onBookMeeting={() => alert("function to book a meeting")}
    />
  </ThemeProvider>
)

HybridHomePageComposition.canvas = {
  width: 1200,
  height: 400,
  overflow: "scroll",
}
