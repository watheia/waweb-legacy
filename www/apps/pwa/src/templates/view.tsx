import React from "react"
import { get } from "lodash"
import { graphql, PageProps } from "gatsby"

import { Layout } from "@waweb/navigator"

// this minimal GraphQL query ensures that when 'gatsby develop' is running,
// any changes to content files are reflected in browser
export const query = graphql`
  query ($url: String) {
    sitePage(path: { eq: $url }) {
      id
    }
  }
`

export const View = ({ children, pageContext, ...props }) => (
  <Layout pageContext={pageContext} {...props}>
    <article className="page py-5 py-sm-6">
      <div className="container container--medium">
        <header className="page__header">
          <h1 className="page__title">
            {get(pageContext, "frontmatter.title", null)}
          </h1>
        </header>
        <div className="page__body">{children}</div>
      </div>
    </article>
  </Layout>
)

export default View
