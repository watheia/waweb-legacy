/** @format */

import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import { Grid, TextField, Button } from "@material-ui/core"
import { SectionHeader } from "@waweb/molecules"
import { CardBase } from "@waweb/organisms"

const useStyles = makeStyles((theme) => ({
  cardBase: {
    background: theme.palette.primary.main,
    [theme.breakpoints.up("md")]: {
      background: `url(https://cdn.watheia.org/assets/illustrations/newsletter-bg.svg) no-repeat 150% 50% ${theme.palette.primary.main}`,
    },
  },
  textWhite: {
    color: "white",
  },
}))

const Subscription = ({
  className,
  ...rest
}: ViewComponentProps): JSX.Element => {
  const classes = useStyles()

  return (
    <div className={className} {...rest}>
      <CardBase
        variant="outlined"
        liftUp
        className={classes.cardBase}
        data-aos="fade-up"
      >
        <>
          <Grid container>
            <Grid item xs={12} md={6}>
              <SectionHeader
                title={
                  <span className={classes.textWhite}>
                    Subscribe To Our Newsletter
                  </span>
                }
                subtitle={
                  <span className={classes.textWhite}>
                    Don't lose a chance to be among the firsts to know about our
                    upcoming news and updates.
                  </span>
                }
                fadeUp
                align="left"
              />
              <Grid container spacing={1} alignItems="center">
                <Grid item xs={12} sm={9}>
                  <TextField
                    size="small"
                    fullWidth
                    color="secondary"
                    label="Email"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12} sm={3}>
                  <Button
                    fullWidth
                    variant="contained"
                    color="secondary"
                    size="large"
                  >
                    send
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </>
      </CardBase>
    </div>
  )
}

export default Subscription
