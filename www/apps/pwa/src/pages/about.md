---
stackbit_url_path: /about
template: advanced
seo:
  extra:
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: website
      keyName: property
      name: 'og:type'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: About
      keyName: property
      name: 'og:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: This is the about us page
      keyName: property
      name: 'og:description'
    - type: stackbit_page_meta_extra
      relativeUrl: true
      value: images/about-1.jpg
      keyName: property
      name: 'og:image'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: summary_large_image
      keyName: name
      name: 'twitter:card'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: About
      keyName: name
      name: 'twitter:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: This is the about us page
      keyName: name
      name: 'twitter:description'
    - type: stackbit_page_meta_extra
      relativeUrl: true
      value: images/about-1.jpg
      keyName: name
      name: 'twitter:image'
  type: stackbit_page_meta
  title: About
  description: This is the about us page
sections:
  - background_image_repeat: no-repeat
    background_image_position: center center
    type: hero_section
    background_image_size: cover
    background_color: secondary
    padding_bottom: medium
    has_border: false
    media_width: fifty
    padding_top: medium
    align: center
    media_position: top
    actions: []
    title: About
    subtitle: About Page Demo
  - features:
      - align: left
        media_position: right
        type: feature
        media_width: fifty
        image_alt: People in conversation in the office lounge area
        actions: []
        content: >-
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nisl
          ligula, cursus id molestie vel, maximus aliquet risus. Vivamus in nibh
          fringilla, fringilla.
        image: 'https://www.datocms-assets.com/49813/1623787047-about-5.jpg'
        title: Section Title One
        subtitle: 'Odio et tortor laoreet, sed interdum augue ornare. '
      - align: left
        media_position: left
        type: feature
        media_width: fifty
        image_alt: People in a conference room
        actions: []
        content: >-
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nisl
          ligula, cursus id molestie vel, maximus aliquet risus. Vivamus in nibh
          fringilla, fringilla.
        image: 'https://www.datocms-assets.com/49813/1623787046-about-2.jpg'
        title: Section Title Two
        subtitle: 'In fermentum odio et tortor laoreet, sed interdum augue ornare. '
      - align: left
        media_position: right
        type: feature
        media_width: fifty
        image_alt: Happy co-workers
        actions: []
        content: >-
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nisl
          ligula, cursus id molestie vel, maximus aliquet risus. Vivamus in nibh
          fringilla, fringilla.
        image: 'https://www.datocms-assets.com/49813/1623787046-about-1.jpg'
        title: Section Title Three
        subtitle: 'Donec nisl ligula, cursus id molestie vel. '
    type: features_section
    background_image_repeat: no-repeat
    background_image_size: cover
    background_image_position: center center
    background_color: none
    has_border: false
    padding_bottom: medium
    padding_top: medium
    align: center
    feature_padding_vert: medium
  - type: grid_section
    background_image_repeat: no-repeat
    background_image_position: center center
    background_image_size: cover
    padding_bottom: medium
    background_color: none
    align: center
    has_border: false
    padding_top: medium
    grid_gap_horiz: large
    enable_cards: true
    grid_gap_vert: large
    grid_cols: two
    grid_items:
      - type: grid_item
        image_has_padding: false
        image_align: left
        image_position: top
        image_width: fifty
        actions_width: auto
        actions_align: left
        actions:
          - type: action
            icon_position: right
            no_follow: false
            new_window: false
            style: link
            has_icon: true
            icon: arrow-right
            label: Get Directions
            url: 'https://goo.gl/maps/eh6fn7JjMS4vYs337'
        content_align: left
        title_align: left
        content: >-
          522 W Riverside Ave<br/> Some Spokane, WA 99201<br/>
          [1-509-873-5879](tel:+15098735879)
        title: Spokane
      - type: grid_item
        image_has_padding: false
        image_align: left
        image_position: top
        image_width: fifty
        actions_width: auto
        actions_align: left
        actions:
          - type: action
            icon_position: right
            no_follow: false
            new_window: false
            style: link
            has_icon: true
            icon: arrow-right
            label: Get Directions
            url: 'https://goo.gl/maps/eh6fn7JjMS4vYs337'
        content_align: left
        title_align: left
        content: >-
          207 N Oregon Ave.<br/> Pasco, WA 99301<br/>
          [1-509-378-7207](tel:+15093787207)
        title: Tri-Cities
    actions: []
    title: Our Officess
  - background_image_repeat: no-repeat
    type: cta_section
    background_image_position: center center
    background_image_size: cover
    has_border: false
    padding_bottom: medium
    background_color: primary
    padding_top: medium
    actions_width: fourty
    align: center
    actions:
      - type: action
        icon_position: right
        no_follow: false
        new_window: false
        style: primary
        has_icon: false
        label: Compare Plans
        url: /pricing
    content: Ut egestas elementum suscipit. Quisque at hendrerit mauris.
    actions_position: bottom
    title: This is a Call to Action
title: About
---
