---
stackbit_url_path: /contact
template: advanced
seo:
  extra:
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: website
      keyName: property
      name: 'og:type'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: Contact
      keyName: property
      name: 'og:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: This is the contact page
      keyName: property
      name: 'og:description'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: summary
      keyName: name
      name: 'twitter:card'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: Contact
      keyName: name
      name: 'twitter:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: This is the contact page
      keyName: name
      name: 'twitter:description'
  type: stackbit_page_meta
  title: Contact
  description: This is the contact page
sections:
  - background_image_repeat: no-repeat
    background_image_position: center center
    type: hero_section
    background_image_size: cover
    background_color: none
    padding_bottom: small
    has_border: true
    media_width: fifty
    padding_top: small
    align: center
    media_position: top
    actions: []
    title: Get in touch
    subtitle: >-
      Have a question? Send us a note using the form below and we will be in
      touch soon.
  - type: grid_section
    background_image_repeat: no-repeat
    background_image_position: center center
    background_image_size: cover
    padding_bottom: large
    background_color: primary
    align: center
    has_border: false
    padding_top: large
    grid_gap_horiz: medium
    enable_cards: true
    grid_gap_vert: medium
    grid_cols: three
    grid_items:
      - type: grid_item
        image_has_padding: true
        image_align: center
        image_position: top
        image_width: fifty
        image_alt: Partnering and sponsorships icon
        actions_width: auto
        actions_align: center
        image: 'https://www.datocms-assets.com/49813/1623787048-icon-1.svg'
        actions:
          - type: action
            icon_position: right
            no_follow: false
            new_window: false
            style: link
            has_icon: true
            icon: arrow-right
            label: Get Support
            url: /general-enquiries
        content_align: center
        title_align: center
        content: >-
          Ac felis donec et odio pellentesque. Sagittis vitae et leo duis ut
          diam.
        title: Partnering and Sponsorships
      - type: grid_item
        image_has_padding: true
        image_align: center
        image_position: top
        image_width: fifty
        image_alt: Help and support icon
        actions_width: auto
        actions_align: center
        image: 'https://www.datocms-assets.com/49813/1623787048-icon-2.svg'
        actions:
          - type: action
            icon_position: right
            no_follow: false
            new_window: false
            style: secondary
            has_icon: true
            icon: arrow-right
            label: Get Support
            url: /general-enquiries
        content_align: center
        title_align: center
        content: >-
          Ac felis donec et odio pellentesque. Sagittis vitae et leo duis ut
          diam quam nulla.
        title: Help & Support
      - type: grid_item
        image_has_padding: true
        image_align: center
        image_position: top
        image_width: fifty
        image_alt: Other queries icon
        actions_width: auto
        actions_align: center
        image: 'https://www.datocms-assets.com/49813/1623787048-icon-3.svg'
        actions:
          - type: action
            icon_position: right
            no_follow: false
            new_window: false
            style: primary
            has_icon: true
            icon: arrow-right
            label: Get Support
            url: /general-enquiries
        content_align: center
        title_align: center
        content: >-
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nisl
          ligula, cursus id molestie vel.
        title: Other Queries
    actions: []
title: Contact
---
