---
seo:
  extra:
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: article
      keyName: property
      name: 'og:type'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: Cur Ipse Pythagoras Et Aegyptum Lustravit
      keyName: property
      name: 'og:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: 'Quis est, qui non oderit libidinosam, protervam adolescentiam'
      keyName: property
      name: 'og:description'
    - type: stackbit_page_meta_extra
      relativeUrl: true
      value: images/classic/post-3.png
      keyName: property
      name: 'og:image'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: summary_large_image
      keyName: name
      name: 'twitter:card'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: Cur Ipse Pythagoras Et Aegyptum Lustravit
      keyName: name
      name: 'twitter:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: 'Quis est, qui non oderit libidinosam, protervam adolescentiam'
      keyName: name
      name: 'twitter:description'
    - type: stackbit_page_meta_extra
      relativeUrl: true
      value: images/classic/post-3.png
      keyName: name
      name: 'twitter:image'
  type: stackbit_page_meta
  title: Cur Ipse Pythagoras Et Aegyptum Lustravit
  description: 'Quis est, qui non oderit libidinosam, protervam adolescentiam'
stackbit_url_path: /blog/post-3
template: post
image: 'https://www.datocms-assets.com/49813/1623787049-post-3.png'
image_alt: Post 3 placeholder image
image_position: top
thumb_image_alt: Post 3 placeholder image
thumb_image: 'https://www.datocms-assets.com/49813/1623787049-post-3.png'
excerpt: >-
  Quis est, qui non oderit libidinosam, protervam adolescentiam? Innumerabilia
  dici possunt in hanc sententiam, sed non necesse est. Putabam equidem satis,
  inquit, me dixisse.
tags:
  - type: tag
    link: blog/tag/stackbit
    title: Stackbit
    id: stackbit
  - type: tag
    link: blog/tag/jamstack
    title: JAMstack
    id: jamstack
date: '2020-05-28'
author:
  photo_alt: Gustav Purpleson
  type: person
  photo: 'https://www.datocms-assets.com/49813/1623787050-gustav-purpleson.jpg'
  first_name: Aaron
  last_name: Miller
  id: amiller
  link: blog/author/amiller
categories:
  - type: category
    title: General
    link: blog/category/general
    id: general
subtitle: >-
  Innumerabilia dici possunt in hanc sententiam, sed non necesse est. Putabam
  equidem satis, inquit, me dixisse.
title: Cur Ipse Pythagoras Et Aegyptum Lustravit
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non est ista, inquam, Piso, magna dissensio. Callipho ad virtutem nihil adiunxit nisi voluptatem, Diodorus vacuitatem doloris. Aut haec tibi, Torquate, sunt vituperanda aut patrocinium voluptatis repudiandum. Invidiosum nomen est, infame, suspectum. Dolor ergo, id est summum malum, metuetur semper, etiamsi non aderit; Duo Reges: constructio interrete. Innumerabilia dici possunt in hanc sententiam, sed non necesse est. Quae fere omnia appellantur uno ingenii nomine, easque virtutes qui habent, ingeniosi vocantur. Ut aliquid scire se gaudeant?

Quis est, qui non oderit libidinosam, protervam adolescentiam? Innumerabilia dici possunt in hanc sententiam, sed non necesse est. Putabam equidem satis, inquit, me dixisse. Quid igitur, inquit, eos responsuros putas? Age, inquies, ista parva sunt. Negabat igitur ullam esse artem, quae ipsa a se proficisceretur;

Cur ipse Pythagoras et Aegyptum lustravit et Persarum magos adiit? Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. Quorum sine causa fieri nihil putandum est. Sit enim idem caecus, debilis. Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Intrandum est igitur in rerum naturam et penitus quid ea postulet pervidendum; Urgent tamen et nihil remittunt. Quae contraria sunt his, malane? Nihil acciderat ei, quod nollet, nisi quod anulum, quo delectabatur, in mari abiecerat. Atque his de rebus et splendida est eorum et illustris oratio. Haec para/doca illi, nos admirabilia dicamus. Non est ista, inquam, Piso, magna dissensio.
