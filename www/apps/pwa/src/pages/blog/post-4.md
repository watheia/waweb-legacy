---
seo:
  extra:
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: article
      keyName: property
      name: 'og:type'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: Vos Autem Cum Perspicuis Dubia Debeatis Illustrare
      keyName: property
      name: 'og:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: 'Itaque hoc frequenter dici solet a vobis, non intellegere nos'
      keyName: property
      name: 'og:description'
    - type: stackbit_page_meta_extra
      relativeUrl: true
      value: images/classic/post-4.png
      keyName: property
      name: 'og:image'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: summary_large_image
      keyName: name
      name: 'twitter:card'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: Vos Autem Cum Perspicuis Dubia Debeatis Illustrare
      keyName: name
      name: 'twitter:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: 'Itaque hoc frequenter dici solet a vobis, non intellegere nos'
      keyName: name
      name: 'twitter:description'
    - type: stackbit_page_meta_extra
      relativeUrl: true
      value: images/classic/post-4.png
      keyName: name
      name: 'twitter:image'
  type: stackbit_page_meta
  title: Vos Autem Cum Perspicuis Dubia Debeatis Illustrare
  description: 'Itaque hoc frequenter dici solet a vobis, non intellegere nos'
stackbit_url_path: /blog/post-4
template: post
image: 'https://www.datocms-assets.com/49813/1623787049-post-4.png'
image_alt: Post 4 placeholder image
image_position: right
thumb_image_alt: Post 4 placeholder image
thumb_image: 'https://www.datocms-assets.com/49813/1623787049-post-4.png'
excerpt: >-
  Itaque hoc frequenter dici solet a vobis, non intellegere nos, quam dicat
  Epicurus voluptatem. Sin kakan malitiam dixisses, ad aliud nos unum certum
  vitium consuetudo Latina traduceret.
tags:
  - type: tag
    link: blog/tag/jamstack
    title: JAMstack
    id: jamstack
  - type: tag
    link: blog/tag/sourcebit
    title: Sourcebit
    id: sourcebit
date: '2020-05-30'
author:
  photo_alt: Dianne Ameter
  type: person
  photo: 'https://www.datocms-assets.com/49813/1623787049-dianne-ameter.jpg'
  first_name: Dianne
  last_name: Ameter
  id: dameter
  link: blog/author/dameter
categories:
  - type: category
    title: Case Studies
    link: blog/category/cases
    id: cases
  - type: category
    title: News
    link: blog/category/news
    id: news
subtitle: >-
  Negat esse eam, inquit, propter se expetendam. Ergo, si semel tristior
  effectus est, hilara vita amissa est.
title: Vos Autem Cum Perspicuis Dubia Debeatis Illustrare
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Teneo, inquit, finem illi videri nihil dolere. Quid est enim aliud esse versutum. Age, inquies, ista parva sunt. Verum hoc idem saepe faciamus. Atque ab his initiis profecti omnium virtutum et originem et progressionem persecuti sunt. Duo Reges: constructio interrete. Itaque eos id agere, ut a se dolores, morbos, debilitates repellant. Estne, quaeso, inquam, sitienti in bibendo voluptas? Iam in altera philosophiae parte. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L.

- Restinguet citius, si ardentem acceperit.
- Te enim iudicem aequum puto, modo quae dicat ille bene noris.
- Quid, quod homines infima fortuna, nulla spe rerum gerendarum, opifices denique delectantur historia?
- Quo minus animus a se ipse dissidens secumque discordans gustare partem ullam liquidae voluptatis et liberae potest.

**Quippe: habes enim a rhetoribus;** Vos autem cum perspicuis dubia debeatis illustrare, dubiis perspicua conamini tollere. Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali. Sic consequentibus vestris sublatis prima tolluntur. Negat esse eam, inquit, propter se expetendam. Ergo, si semel tristior effectus est, hilara vita amissa est.
