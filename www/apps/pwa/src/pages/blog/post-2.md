---
seo:
  extra:
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: article
      keyName: property
      name: 'og:type'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: 'Non Minor, Inquit, Voluptas Percipitur Ex Vilissimis'
      keyName: property
      name: 'og:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: Quae diligentissime contra Aristonem dicuntur a Chryippo
      keyName: property
      name: 'og:description'
    - type: stackbit_page_meta_extra
      relativeUrl: true
      value: images/classic/post-2.png
      keyName: property
      name: 'og:image'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: summary_large_image
      keyName: name
      name: 'twitter:card'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: 'Non Minor, Inquit, Voluptas Percipitur Ex Vilissimis'
      keyName: name
      name: 'twitter:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: Quae diligentissime contra Aristonem dicuntur a Chryippo
      keyName: name
      name: 'twitter:description'
    - type: stackbit_page_meta_extra
      relativeUrl: true
      value: images/classic/post-2.png
      keyName: name
      name: 'twitter:image'
  type: stackbit_page_meta
  title: 'Non Minor, Inquit, Voluptas Percipitur Ex Vilissimis'
  description: Quae diligentissime contra Aristonem dicuntur a Chryippo
stackbit_url_path: /blog/post-2
template: post
image: 'https://www.datocms-assets.com/49813/1623787048-post-2.png'
image_alt: Post 2 placeholder image
image_position: left
thumb_image_alt: Post 2 placeholder image
thumb_image: 'https://www.datocms-assets.com/49813/1623787048-post-2.png'
excerpt: >-
  Quae diligentissime contra Aristonem dicuntur a Chryippo. Videamus igitur
  sententias eorum, tum ad verba redeamus. Levatio igitur vitiorum magna.
tags:
  - type: tag
    link: blog/tag/jamstack
    title: JAMstack
    id: jamstack
  - type: tag
    link: blog/tag/netlify
    title: Netlify
    id: netlify
date: '2020-05-25'
author:
  photo_alt: Hilary Ouse
  type: person
  photo: 'https://www.datocms-assets.com/49813/1623787050-hugh-saturation.jpg'
  first_name: Patrick
  last_name: Stevenson
  id: pstevenson
  link: blog/author/pstevenson
categories:
  - type: category
    title: News
    link: blog/category/news
    id: news
  - type: category
    title: General
    link: blog/category/general
    id: general
subtitle: >-
  Idemque diviserunt naturam hominis in animum et corpus. Si enim ad populum me
  vocas, eum.
title: 'Non Minor, Inquit, Voluptas Percipitur Ex Vilissimis'
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Negat esse eam, inquit, propter se expetendam. Nam de isto magna dissensio est. Nullum inveniri verbum potest quod magis idem declaret Latine, quod Graece, quam declarat voluptas. Idemne potest esse dies saepius, qui semel fuit? Non est ista, inquam, Piso, magna dissensio. Ita multo sanguine profuso in laetitia et in victoria est mortuus. Quasi vero, inquit, perpetua oratio rhetorum solum, non etiam philosophorum sit. Duo Reges: constructio interrete.

Quae diligentissime contra Aristonem dicuntur a Chryippo. Videamus igitur sententias eorum, tum ad verba redeamus. Levatio igitur vitiorum magna fit in iis, qui habent ad virtutem progressionis aliquantum. Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet. Nihil enim hoc differt. Quorum sine causa fieri nihil putandum est.

## Sed Nimis Multa

An potest cupiditas finiri? Quamquam te quidem video minime esse deterritum. Sit, inquam, tam facilis, quam vultis, comparatio voluptatis, quid de dolore dicemus? Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt.

> Ita est quoddam commune officium sapientis et insipientis, ex quo efficitur versari in iis, quae media dicamus.

Omnes enim iucundum motum, quo sensus hilaretur. Hoc est non dividere, sed frangere. Sed ne, dum huic obsequor, vobis molestus sim. Efficiens dici potest. Quia dolori non voluptas contraria est, sed doloris privatio. Quid dubitas igitur mutare principia naturae? Atqui eorum nihil est eius generis, ut sit in fine atque extrerno bonorum. Idemque diviserunt naturam hominis in animum et corpus. Si enim ad populum me vocas, eum. Sed tu istuc dixti bene Latine, parum plane.

Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. **Audeo dicere, inquit.** Perturbationes autem nulla naturae vi commoventur, omniaque ea sunt opiniones ac iudicia levitatis. Nunc haec primum fortasse audientis servire debemus. Nihil ad rem! Ne sit sane; Et certamen honestum et disputatio splendida! omnis est enim de virtutis dignitate contentio. Quonam, inquit, modo? Nullum inveniri verbum potest quod magis idem declaret Latine, quod Graece, quam declarat voluptas. Hoc ille tuus non vult omnibusque ex rebus voluptatem quasi mercedem exigit.

- Fortitudinis quaedam praecepta sunt ac paene leges, quae effeminari - virum vetant in dolore.
- Non igitur bene.

Maximas vero virtutes iacere omnis necesse est voluptate dominante.
