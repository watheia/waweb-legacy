import { content } from "./data/content"
import { similarStories } from "./data/similarStories"
import { sidebarArticles } from "./data/sidebarArticles"
import { integrations } from "./data/integrations"
import { reviews } from "./data/reviews"
import { sitemap } from "./data/sitemap"
import * as samples from "./data/samples"

export const ctx = {
  content,
  sidebarArticles,
  integrations,
  reviews,
  similarStories,
  samples,
  sitemap,
}
export default ctx
