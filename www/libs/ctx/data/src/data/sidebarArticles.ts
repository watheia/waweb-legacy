export const sidebarArticles = [
  {
    cover: {
      src: "https://cdn.watheia.org/assets/photos/blog/cover4.jpg",
      srcSet: "https://cdn.watheia.org/assets/photos/blog/cover4.jpg 2x",
    },
    title: "LARQ | World's First Self-cleaning Water Bottle‎",
    author: {
      photo: {
        src: "https://cdn.watheia.org/assets/photos/people/kate-segelson.jpg",
        srcSet:
          "https://cdn.watheia.org/assets/photos/people/kate-segelson@2x.jpg 2x",
      },
      name: "Kate Segelson",
    },
    date: "04 Aug",
    tags: ["larq", "bottle", "shop", "drinks", "eco", "self washing"],
  },
  {
    cover: {
      src: "https://cdn.watheia.org/assets/photos/blog/cover3.jpg",
      srcSet: "https://cdn.watheia.org/assets/photos/blog/cover3.jpg 2x",
    },
    title: "NIKE Online Store launches the website‎",
    author: {
      photo: {
        src: "https://cdn.watheia.org/assets/photos/people/jack-smith.jpg",
        srcSet:
          "https://cdn.watheia.org/assets/photos/people/jack-smith@2x.jpg 2x",
      },
      name: "Jack Smith",
    },
    date: "04 Aug",
    tags: ["nike", "sport", "shop", "training"],
  },
  {
    cover: {
      src: "https://cdn.watheia.org/assets/photos/blog/cover2.jpg",
      srcSet: "https://cdn.watheia.org/assets/photos/blog/cover2.jpg 2x",
    },
    title: "Adidas will release your favourite shoes",
    author: {
      photo: {
        src: "https://cdn.watheia.org/assets/photos/people/akachi-luccini.jpg",
        srcSet:
          "https://cdn.watheia.org/assets/photos/people/akachi-luccini@2x.jpg 2x",
      },
      name: "Akachi Luccini",
    },
    date: "04 Aug",
    tags: ["adidas", "sport", "shop", "training"],
  },
  {
    cover: {
      src: "https://cdn.watheia.org/assets/photos/blog/cover1.jpg",
      srcSet: "https://cdn.watheia.org/assets/photos/blog/cover1.jpg 2x",
    },
    title: "Simple approach to Australian cafe",
    author: {
      photo: {
        src: "https://cdn.watheia.org/assets/photos/people/veronica-adams.jpg",
        srcSet:
          "https://cdn.watheia.org/assets/photos/people/veronica-adams@2x.jpg 2x",
      },
      name: "Veronica Adams",
    },
    date: "04 Aug",
    tags: ["coffee", "cups", "morning coffee", "breakfast"],
  },
]
