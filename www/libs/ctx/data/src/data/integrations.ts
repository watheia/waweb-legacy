export const integrations = [
  {
    logo: "https://cdn.watheia.org/assets/logos/slack.svg",
    name: "Slack",
  },
  {
    logo: "https://cdn.watheia.org/assets/logos/mailchimp.svg",
    name: "Mailchimp",
  },
  {
    logo: "https://cdn.watheia.org/assets/logos/dropbox.svg",
    name: "Dropbox",
  },
  {
    logo: "https://cdn.watheia.org/assets/logos/google-drive.svg",
    name: "Google Drive",
  },
  {
    logo: "https://cdn.watheia.org/assets/logos/google-ad-manager.svg",
    name: "Google Ad Manager",
  },
  {
    logo: "https://cdn.watheia.org/assets/logos/atlassian.svg",
    name: "Atlassian",
  },
]
