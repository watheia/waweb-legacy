import React, { HTMLAttributes } from "react"
import { get } from "lodash"

import { Link, withPrefix } from "@waweb/utils"
import Icon from "@watheia/com.ui.atoms.icon"
import HeaderMenu from "./HeaderMenu"

export type HeaderProps = {
  pageContext?: unknown
} & HTMLAttributes<HTMLDivElement>

export const Header = ({ pageContext, ...props }: HeaderProps) => {
  const data = get(pageContext, "site.siteMetadata.header", null)
  return (
    <header className="site-header py-2" {...props}>
      {data && (
        <div className="container">
          <nav
            className="navbar flex items-center"
            aria-label="Main Navigation"
          >
            <Link className="sr-only" to="#content">
              Skip to main content
            </Link>
            <div className="navbar__branding mr-2">
              {get(data, "logo", null) ? (
                <Link className="navbar__logo m-0" to={withPrefix("/")}>
                  <img
                    src={withPrefix(get(data, "logo", null))}
                    alt={get(data, "logo_alt", null)}
                  />
                </Link>
              ) : (
                <Link className="navbar__title h4 m-0" to={withPrefix("/")}>
                  {get(data, "title", null)}
                </Link>
              )}
            </div>
            {(get(data, "has_primary_nav", null) ||
              get(data, "has_secondary_nav", null)) && (
              <React.Fragment>
                <div className="navbar__container flex-md-auto">
                  <div className="navbar__scroller">
                    <div className="navbar__inner">
                      <button
                        aria-label="Close"
                        className="btn btn--icon btn--clear navbar__close-btn js-nav-toggle"
                      >
                        <Icon {...props} fontIconClass={"close"} />
                        <span className="sr-only">Close</span>
                      </button>
                      <div className="navbar__menu flex-md">
                        {get(data, "has_primary_nav", null) &&
                          get(data, "primary_nav_links", null) && (
                            <HeaderMenu
                              {...props}
                              header_menu={get(data, "primary_nav_links", null)}
                              page={pageContext}
                            />
                          )}
                        {get(data, "has_secondary_nav", null) &&
                          get(data, "secondary_nav_links", null) && (
                            <HeaderMenu
                              {...data}
                              header_menu={get(
                                data,
                                "secondary_nav_links",
                                null
                              )}
                              page={pageContext}
                            />
                          )}
                      </div>
                    </div>
                  </div>
                </div>
                <button
                  aria-label="Menu"
                  className="btn btn--icon btn--clear navbar__menu-btn js-nav-toggle ml-auto"
                >
                  <Icon {...props} fontIconClass={"menu"} />
                  <span className="sr-only">Menu</span>
                </button>
              </React.Fragment>
            )}
          </nav>
        </div>
      )}
    </header>
  )
}

export default Header
