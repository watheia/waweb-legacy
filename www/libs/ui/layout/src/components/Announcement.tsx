import React from "react"
import { get } from "lodash"

import { classNames, markdownify } from "@waweb/utils"
import Icon from "@watheia/com.ui.atoms.icon"

export class Announcement extends React.Component {
  props: any
  render() {
    const site = get(this.props, "site", null)
    const align_x =
      get(site, "siteMetadata.header.anncmnt_align", null) || "left"
    const has_close_btn = get(
      site,
      "siteMetadata.header.anncmnt_has_close",
      null
    )
    return (
      <div
        className={classNames("announcement-bar", "py-2", {
          "js-announcement": has_close_btn,
          "is-hidden": has_close_btn,
        })}
        {...(has_close_btn
          ? {
              "data-anncmnt-id": get(
                site,
                "siteMetadata.header.anncmnt_id",
                null
              ),
            }
          : null)}
      >
        <div className="container">
          <div className="announcement-bar__content">
            <div
              className={classNames("announcement-bar__copy", {
                "text-center": align_x === "center",
                "text-right": align_x === "right",
              })}
            >
              {markdownify(
                get(site, "siteMetadata.header.anncmnt_content", null)
              )}
            </div>
            {has_close_btn && (
              <button
                aria-label="Close"
                className="btn btn--icon btn--clear js-announcment-close"
              >
                <Icon {...this.props} fontIconClass="close" />
                <span className="sr-only">Close</span>
              </button>
            )}
          </div>
        </div>
      </div>
    )
  }
}

export default Announcement
