import React from "react"
import { map, trim, get } from "lodash"

import { classNames } from "@waweb/utils"
import Action from "./Action"

export type HeaderMenuProps = {
  page?: unknown
  header_menu?: Record<string, unknown>
}

export const HeaderMenu = ({
  page,
  header_menu,
  ...props
}: HeaderMenuProps): JSX.Element => {
  return (
    <ul className="menu flex-md items-md-center">
      {map(header_menu, (item, item_idx) => {
        const page_url = trim(get(page, "url", null), "/")
        const item_url = trim(get(item, "url", null), "/")
        const item_style = get(item, "style", null) || "link"
        return (
          <li
            key={item_idx}
            className={classNames("menu__item", "ml-md-3", {
              "is-active": page_url === item_url && item_style === "link",
              "menu__item-btn": item_style !== "link",
            })}
          >
            <Action {...props} action={item as any} />
          </li>
        )
      })}
    </ul>
  )
}

export default HeaderMenu
