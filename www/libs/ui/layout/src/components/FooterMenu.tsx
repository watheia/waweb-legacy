import React from "react"
import { map } from "lodash"

import Action from "./Action"

export type FooterMenuProps = {
  page?: unknown
  footer_menu?: Record<string, unknown>
}

export const FooterMenu = ({
  footer_menu,
  ...props
}: FooterMenuProps): JSX.Element => (
  <ul className="menu">
    {map(footer_menu, (menu_item, menu_item_idx) => (
      <li key={menu_item_idx} className="menu__item mb-1">
        <Action {...props} action={menu_item as any} />
      </li>
    ))}
  </ul>
)
