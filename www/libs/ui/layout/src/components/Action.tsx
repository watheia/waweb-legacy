import React, { HTMLAttributes } from "react"
import { get } from "lodash"

import { Link, withPrefix, classNames } from "@waweb/utils"
import Icon from "@watheia/com.ui.atoms.icon"

type ActionProps = {
  action?: Record<string, unknown>
} & HTMLAttributes<HTMLDivElement>

export const Action = ({ action, ...props }: ActionProps) => {
  const action_style = get(action, "style", null) || "link"
  const action_icon_pos = get(action, "icon_position", null) || "right"
  return (
    <Link
      to={withPrefix(get(action, "url", "/") as string)}
      {...(get(action, "new_window", null) ? { target: "_blank" } : null)}
      {...(get(action, "new_window", null) || get(action, "no_follow", null)
        ? {
            rel:
              (get(action, "new_window", null) ? "noopener " : "") +
              (get(action, "no_follow", null) ? "nofollow" : ""),
          }
        : null)}
      className={classNames({
        btn:
          action_style === "primary" ||
          action_style === "secondary" ||
          get(action, "has_icon", null),
        "btn--primary": action_style === "primary",
        "btn--secondary": action_style === "secondary",
        "btn--icon":
          get(action, "has_icon", null) && action_icon_pos === "center",
        "btn--clear": get(action, "has_icon", null) && action_style === "link",
      })}
    >
      {get(action, "has_icon", null) ? (
        <React.Fragment>
          <Icon
            {...props}
            fontIconClass={get(action, "icon", "asterisk") as string}
          />
          <span
            className={classNames({
              "order-first": action_icon_pos === "right",
              "sr-only": action_icon_pos === "center",
            })}
          >
            {get(action, "label", null) as string | null}
          </span>
        </React.Fragment>
      ) : (
        get(action, "label", null)
      )}
    </Link>
  )
}

export default Action
