import React, { HTMLAttributes } from "react"
import { Helmet } from "react-helmet"
import { get, join, map, trim } from "lodash"

import { withPrefix, attribute } from "@waweb/utils"
import { Announcement } from "./Announcement"
import { Header } from "./Header"
import { Footer } from "./Footer"
import { ThemeProvider } from "@waweb/uikit.theme.theme-provider"

export type LayoutProps = {
  pageContext: unknown
} & HTMLAttributes<HTMLDivElement>

export const Layout = ({
  pageContext,
  children,
  ...props
}: LayoutProps): JSX.Element => {
  const style = get(pageContext, "site.siteMetadata.style", null) || "classic"
  const font =
    get(pageContext, "site.siteMetadata.base_font", null) || "sans-serif"
  return (
    <ThemeProvider colorScheme="light">
      <Helmet>
        <title>
          {get(pageContext, "frontmatter.seo.title", null)
            ? get(pageContext, "frontmatter.seo.title", null)
            : get(pageContext, "frontmatter.title", null) +
              " | " +
              get(pageContext, "site.siteMetadata.title", null)}
        </title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initialScale=1.0" />
        <meta name="google" content="notranslate" />
        <meta
          name="description"
          content={get(pageContext, "frontmatter.seo.description", null) || ""}
        />
        {get(pageContext, "frontmatter.seo.robots", null) && (
          <meta
            name="robots"
            content={join(
              get(pageContext, "frontmatter.seo.robots", null),
              ","
            )}
          />
        )}
        {map(
          get(pageContext, "frontmatter.seo.extra", null),
          (meta, meta_idx) => {
            const key_name = get(meta, "keyName", null) || "name"
            return get(meta, "relativeUrl", null) ? (
              get(pageContext, "site.siteMetadata.domain", null) &&
                (() => {
                  const domain = trim(
                    get(pageContext, "site.siteMetadata.domain", null),
                    "/"
                  )
                  const rel_url = withPrefix(get(meta, "value", null))
                  const full_url = domain + rel_url
                  return (
                    <meta
                      key={meta_idx}
                      {...attribute(key_name, get(meta, "name", null))}
                      content={full_url}
                    />
                  )
                })()
            ) : (
              <meta
                key={meta_idx + ".1"}
                {...attribute(key_name, get(meta, "name", null))}
                content={get(meta, "value", null)}
              />
            )
          }
        )}
        {get(pageContext, "site.siteMetadata.favicon", null) && (
          <link
            rel="icon"
            href={withPrefix(
              get(pageContext, "site.siteMetadata.favicon", null)
            )}
          />
        )}
        <body
          className={
            "layout-" +
            get(pageContext, "site.siteMetadata.layout_type", null) +
            " style-" +
            get(pageContext, "site.siteMetadata.style", null) +
            " palette-" +
            get(pageContext, "site.siteMetadata.palette", null) +
            " mode-" +
            get(pageContext, "site.siteMetadata.mode", null) +
            " font-" +
            get(pageContext, "site.siteMetadata.base_font", null)
          }
        />
      </Helmet>

      <div id="site-wrap" className="site">
        {get(pageContext, "site.siteMetadata.header.has_anncmnt", null) &&
          get(pageContext, "site.siteMetadata.header.anncmnt_content", null) &&
          (get(
            pageContext,
            "site.siteMetadata.header.anncmnt_is_home_only",
            null
          ) ? (
            get(pageContext, "url", null) === "/" && (
              <Announcement {...props} site={get(pageContext, "site", null)} />
            )
          ) : (
            <Announcement {...props} site={get(pageContext, "site", null)} />
          ))}
        <Header {...props} />
        <main id="content" className="site-content">
          {children}
        </main>
        <Footer {...props} />
      </div>
      {(get(
        props,
        "pageContext.site.siteMetadata.header.has_primary_nav",
        null
      ) ||
        get(
          props,
          "pageContext.site.siteMetadata.header.has_secondary_nav",
          null
        )) && <div className="nav-overlay js-nav-toggle" />}
    </ThemeProvider>
  )
}

export default Layout
